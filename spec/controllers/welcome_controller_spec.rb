require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #result" do
    it "returns splited @array" do
      post :result, array: 'Saab Volvo, BMW Saab, 1, 2, 2'
      splited_array = ['Saab', 'Volvo', 'BMW', 'Saab', '1', '2', '2']
      expect(assigns(:array)).to eq(splited_array)
    end
  end

  describe "POST #result" do
    it "returns empty page if @array is empty" do
      post :result
      expect(response.status).to eq(200)
    end
  end

end
